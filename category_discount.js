/**
 * Created by Burlakovmax on 15.05.2017.
 */
jQuery(document).ready(function($) {
    $(".form-item-quantity").click(function(){
       var qnt = $("#edit-quantity",this).attr('aria-valuenow');
       var nid = $("#edit-quantity",this).attr('nid');

        $.post("/ajax/item_count", {qnt: qnt,nid: nid}, function(result){
            $('.field-name-commerce-price b').html(result.price);
            console.log(result);
        });
    });
});